FROM ubuntu:14.04

MAINTAINER tobiaslang86@googlemail.com

LABEL Description="PHP7 Base Container"

# # # # # # # # # # # # # # # # # # #  set timezone to Europe/Berlin
RUN ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

## install PHP7 
RUN apt-get update && \ 
    apt-get install -y software-properties-common python-software-properties curl && \ 
    add-apt-repository -y ppa:ondrej/php-7.0 && \ 
    apt-get update && \ 
    apt-get install -y --force-yes php7.0 php7.0-sqlite

## install composer
RUN curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer

## cleanup junkfiles
RUN apt-get purge -y software-properties-common python-software-properties curl && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*